package adt.heap.extended;

import java.util.Comparator;

import adt.heap.HeapImpl;

public class FloorCeilHeapImpl extends HeapImpl<Integer> implements FloorCeilHeap {

	public FloorCeilHeapImpl(Comparator<Integer> comparator) {
		super(comparator);
	}

	@Override
	public Integer floor(Integer[] array, double numero) {
		Integer floor = null;
		if (array != null) {
			for (Integer value : array) {
				insert(value);
			}
			floor = floor(numero);
		}
		return floor;
	}

	private Integer floor(double numero) {
		Integer floor = null;
		Integer root = extractRootElement();
		if (root != null) {
			if (root == numero) {
				floor = root;
			} else if (root < numero) {
				floor = floor(numero);
				if (floor == null || floor < root) {
					floor = root;
				}
			} else {
				floor = floor(numero);
			}
		}
		return floor;
	}

	@Override
	public Integer ceil(Integer[] array, double numero) {
		Integer ceil = null;
		if (array != null) {
			for (Integer value : array) {
				insert(value);
			}
			ceil = ceil(numero);
		}
		return ceil;
	}

	private Integer ceil(double numero) {
		Integer ceil = null;
		Integer root = extractRootElement();
		if (root != null) {
			if (root == numero) {
				ceil = root;
			} else if (root > numero) {
				ceil = ceil(numero);
				if (ceil == null || ceil > root) {
					ceil = root;
				}
			} else {
				ceil = ceil(numero);
			}
		}
		return ceil;
	}

}
