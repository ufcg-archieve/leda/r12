# Roteiro 12
### Heap Binária

- [Heap](https://gitlab.com/ufcg-archieve/leda/r12/-/blob/8dc936a810749e89af0122d7d2814cce12d14e20/src/main/java/adt/heap/HeapImpl.java)
- [FloorCeilHeap](https://gitlab.com/ufcg-archieve/leda/r12/-/blob/190be850c1d92c885cf36d368a908bfdf676ec8f/src/main/java/adt/heap/extended/FloorCeilHeapImpl.java)
- [OrderStatisticsHeap](https://gitlab.com/ufcg-archieve/leda/r12/-/blob/8dc936a810749e89af0122d7d2814cce12d14e20/src/main/java/orderStatistic/OrderStatisticsHeapImpl.java)
